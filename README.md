# Utility scripts around S3 object ownership & ACLs

This is a place to stash some rudimentary scripts used to view or manipulate AWS S3 object ownership and ACLs.  They're things I've used to quickly accomplish a specific task and therefore you are unlikely to find much in the way of
- commenting
- error handling
- argument parsing
- logging
- etc.

## list_wrong_owner_objects.py

Use this to find objects in a bucket, under a specified key prefix, that don't have the expected ownership.  Use the constants at the top to define the inputs:

| Constant | Description |
| ------ | ------ |
| `BUCKET_NAME` | The name of the bucket. |
| `PREFIX` | The prefix under which to begin searching. |
| `CORRECT_OWNER` | The expected owner for objects.  Objects that have correct ownership are ignored. |
| `OUTPUT_FILE` | The file in which keys for objects with incorrect owners will be listed. |
| `LAST_KEY_FILE` | The last key checked.  Useful when the AWS session times out and you want to resume from where you left off rather than starting over.  When that happens you can use the value written to LAST_KEY_FILE as the START_AFTER value for the next run. |
| `START_AFTER` | If this is not None, searching will begin after the specified object key. |

Run the script using aws-vault to manage AWS credentials.  E.g.,

```
aws-vault exec heb-dsol-data-lake-prd-staff -- ./list_wrong_owner_objects.py
```
