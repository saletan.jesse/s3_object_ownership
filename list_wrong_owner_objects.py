#!/usr/bin/env python3

import boto3

BUCKET_NAME = 'heb-dsol-data-science-prd-extract'
PREFIX = 'markdown/inference_data_v2'
CORRECT_OWNER = 'aws.act.heb-dsol-data-lake-prd'
OUTPUT_FILE = 'wrong_owner_keys'
LAST_KEY_FILE = 'last_key_checked'
START_AFTER = None

def get_page_iterator(client, bucket_name, prefix, start_after=None):
    paginator = client.get_paginator('list_objects_v2')

    kwargs = {
        'Bucket': bucket_name,
        'Prefix': prefix,
        'FetchOwner': True
    }

    if start_after:
        kwargs['StartAfter'] = start_after

    page_iterator = paginator.paginate(**kwargs)
    return page_iterator

def list_objects(page_iterator, page_limit=None):
    n = 1
    for page in page_iterator:
        if page_limit and n > page_limit:
            break
        else:
            print(f'Listing objects in page {n}...')
            objects = page['Contents']
            last_key = objects[-1]['Key']
            object_keys = [object['Key'] for object in objects if object['Owner']['DisplayName'] != CORRECT_OWNER]
            append_keys_to_file(object_keys)
            record_last_key(last_key)
            n += 1

def append_keys_to_file(object_keys):
    with open(OUTPUT_FILE, 'a') as f:
        for key in object_keys:
            f.write(f'{key}\n')

def record_last_key(last_key):
    with open(LAST_KEY_FILE, 'w') as f:
        f.write(f'{last_key}\n')

page_iterator = get_page_iterator(
    boto3.client('s3'),
    BUCKET_NAME,
    PREFIX,
    start_after=START_AFTER
)

list_objects(
    page_iterator
)

print('Done!')
